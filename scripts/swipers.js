const reviewsSwiperThumbs = new Swiper('#reviews-slider-thumbnails', {
    observer: true,
    observeParents: true,
    spaceBetween: 10,
    slidesPerView: 3,
    loop: true,
    initialSlide: -1,
    loopedSlides: 20,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});
const reviewsSwiper = new Swiper('#reviews-slider', {
    initialSlide: -1,
    spaceBetween: 10,
    loop: true,
    loopedSlides: 20,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: reviewsSwiperThumbs,
    },
});

setTimeout(() => {reviewsSwiperThumbs.update()},1000);
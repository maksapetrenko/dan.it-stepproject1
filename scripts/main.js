lastScrollY = pageYOffset;
page = {
    nav: {
        DOMelem: document.getElementById('header-nav'),
        isMovable: true,
        initOnScrollToggle() {
            this.DOMelem.classList.add('nav-shown');
            window.onscroll = () => {
                if (this.isMovable) {
                    if (pageYOffset > lastScrollY) {
                        this.isMovable = false;
                        this.DOMelem.classList.remove('nav-shown')
                        this.DOMelem.classList.add('nav-hidden')
                        setTimeout(() =>
                                this.isMovable = true
                            , 500)
                    } else if (pageYOffset < lastScrollY) {
                        this.isMovable = false;
                        this.DOMelem.classList.add('nav-shown');
                        this.DOMelem.classList.remove('nav-hidden')
                        setTimeout(() =>
                                this.isMovable = true
                            , 500)
                    }
                    window.lastScrollY = pageYOffset;
                }
            }
        },
    },
    ourServices: {
        DOMtabsList: document.getElementById('our-services-list'),
        DOMdescriptionContainer: document.getElementById('our-services-description-container'),
        initTabs() {
            this.DOMtabsList.addEventListener('click', event => {
                const chosenTab = event.target;
                if ((chosenTab.dataset.serviceType) && this.DOMdescriptionContainer.dataset.serviceType !== chosenTab.dataset.serviceType) {

                    page.repaintTabs([...this.DOMtabsList.children], chosenTab);

                    const contentStorageCssPath = `#our-services-section .hidden-content-container[data-service-type="${chosenTab.dataset.serviceType}"]`
                    this.DOMdescriptionContainer.innerHTML = document.querySelector(contentStorageCssPath).innerHTML;

                    this.DOMdescriptionContainer.dataset.serviceType = chosenTab.dataset.serviceType;
                }
            })
        },
    },

    ourWorkSamples: {
        maxListedImagesNumber: 12,
        DOMtabsList: document.getElementById('our-works-categories-list'),
        DOMimageGallery: document.getElementById('our-works-samples-gallery'),
        DOMimageDatabase: document.getElementById('our-works-database'),
        DOMloadMoreBtn: document.getElementById('our-works-load-more-btn'),
        get DOMshownImagesArr() {
            return [...this.DOMimageGallery.children];
        },
        imagesDescriptions: {
            DOMimageGallery: document.getElementById('our-works-samples-gallery'),
            DOMdescriptionContainer: document.getElementById('our-works-descriptions-storage'),
            currentImg: null,
            prevImg: null,
            getImgCoords(img) {
                let box = img.getBoundingClientRect();
                return {
                    top: box.top + pageYOffset,
                    left: box.left + pageXOffset
                };
            },

            createNshowDesription(img) {
                const imgCoords = this.getImgCoords(img);
                const imgWidth = getComputedStyle(img).width
                const imgHeight = getComputedStyle(img).height
                const imgProjectName = img.dataset.projectName
                const imgProjectType = img.dataset.projectType

                const descrSignHTML = `<div class="our-work-additional-info d-flex" style="width: ${imgWidth}; height: ${imgHeight}; top: ${imgCoords['top']}px; left: ${imgCoords['left']}px;">
    <div><a href=""><svg class="our-work-additional-info-first-link" viewBox="0 0 15 15" fill="white"
                         xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"/>
    </svg></a>
        <a href=""><div class="our-work-additional-info-second-link"></div></a>
    </div>
    <div>
        <a href=""><h5 class="our-work-additional-info-project-name">${imgProjectName}</h5></a>
        <h6 class="our-work-additional-info-project-type">${imgProjectType}</h6>
    </div>
</div>`
                this.DOMdescriptionContainer.insertAdjacentHTML('beforeend', descrSignHTML)
                const newDescrSign = this.DOMdescriptionContainer.children[this.DOMdescriptionContainer.children.length - 1];
                newDescrSign.classList.add('fading-in');
                setTimeout(() => {
                    newDescrSign.classList.remove('fading-in');
                }, 1000)
            },
            hideNdeleteDescription() {
                const expiredDescrSign = this.DOMdescriptionContainer.children[0];
                if (!expiredDescrSign || !this.prevImg) return;
                expiredDescrSign.classList.add('fading-out');
                setTimeout(() => {
                    expiredDescrSign.remove();
                }, 1000)
            },
            initGalleryListeners() {
                this.DOMimageGallery.addEventListener('mouseover', event => {
                    for (let i = 0; i < this.DOMdescriptionContainer.children.length - 1; i++) {
                        this.DOMdescriptionContainer.children[i].remove();
                    }
                    if (event.target.tagName?.toLowerCase() === 'img' && event.target !== this.currentImg) {
                        this.prevImg = this.currentImg;
                        this.currentImg = event.target;
                        this.createNshowDesription(this.currentImg)
                    } else if (event.target.tagName?.toLowerCase() !== 'img') {
                        this.currentImg = null
                        this.hideNdeleteDescription();
                    }
                })
                this.DOMimageGallery.addEventListener('mouseout', () => {
                    if (this.prevImg) {
                        this.hideNdeleteDescription()
                    }
                })
                window.addEventListener('resize', () => {
                    this.DOMdescriptionContainer.innerHTML = '';
                })
            }
        },

        getImagesArrayByCategory(category) {
            return [...category !== 'all'
                ? document.querySelectorAll(`#our-works-database [data-project-type="${category}"]`)
                : document.querySelectorAll(`#our-works-database img`)]
        },
        repasteImagesByCategory(category) {

            const excludeExistingImages = (imgArr) => {
                const arr = [...imgArr];
                for (const img of this.DOMshownImagesArr) {
                    arr.splice(
                        arr.findIndex(elem =>
                            elem.currentSrc.slice(elem.currentSrc.lastIndexOf('/') + 1) === img.currentSrc.slice(img.currentSrc.lastIndexOf('/') + 1)
                        )
                        , 1)
                }
                return arr;
            }
            const shuffleImageCollection = (imgArr) => {
                return imgArr.sort(() => Math.random() - 0.5);
            }
            const imagesToPasteArray = excludeExistingImages(this.getImagesArrayByCategory(category));
            const shuffledImagesArray = shuffleImageCollection(imagesToPasteArray)

            if (shuffledImagesArray.length > 12) {
                shuffledImagesArray.length = 12
            }

            const fragment = new DocumentFragment();
            for (const elem of shuffledImagesArray) {
                fragment.append(elem.cloneNode(true));
            }
            this.DOMimageGallery.append(fragment)
        },
        initTabs() {
            this.DOMtabsList.addEventListener('click', event => {

                const chosenTab = event.target;
                if ((chosenTab.dataset.projectType) && this.DOMimageGallery.dataset.projectType !== chosenTab.dataset.projectType) {
                    this.maxListedImagesNumber = 12;
                    this.DOMloadMoreBtn.style.display = 'none';
                    this.DOMimageGallery.innerHTML = '';

                    if (this.getImagesArrayByCategory(chosenTab.dataset.projectType).length > this.maxListedImagesNumber) {
                        this.DOMloadMoreBtn.style.display = 'initial';
                    }
                    page.repaintTabs([...this.DOMtabsList.children], chosenTab);
                    this.repasteImagesByCategory(chosenTab.dataset.projectType);

                    this.DOMimageGallery.dataset.projectType = chosenTab.dataset.projectType;
                }
            })
        },
        initLoadMoreButtonOnclickListener() {
            this.DOMloadMoreBtn.addEventListener('click', () => {


                const actualCategory = this.DOMimageGallery.dataset.projectType;
                const areEnoughImagesAvailable = () => {
                    return this.getImagesArrayByCategory(actualCategory).length - this.DOMshownImagesArr.length > 12;
                }
                this.maxListedImagesNumber += 12;

                const normalContent = this.DOMloadMoreBtn.innerHTML;
                this.DOMloadMoreBtn.classList.add('site-loadMore-button-no-decor')
                this.DOMloadMoreBtn.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -2 64 62" fill="white">
            <path class="cls-1"
                  d="M63.22,22.34a2.57,2.57,0,0,0-3.62.06l-3.39,3.52A28.14,28.14,0,0,0,8.05,8.45h0a29.36,29.36,0,0,0-3.52,4.38,2.55,2.55,0,0,0,2.15,4,2.53,2.53,0,0,0,2.14-1.16A24.49,24.49,0,0,1,11.7,12h0A23,23,0,0,1,51.08,25.84l-3.19-3.19a2.56,2.56,0,1,0-3.62,3.62L52,34a2.56,2.56,0,0,0,1.81.75h0a2.56,2.56,0,0,0,1.82-.79L63.28,26A2.57,2.57,0,0,0,63.22,22.34Z"/>
            <path class="cls-1"
                  d="M15.53,47.43a23,23,0,0,1-6.4-6.28A2.56,2.56,0,1,0,4.9,44a28,28,0,0,0,7.82,7.67,2.49,2.49,0,0,0,1.4.42,2.56,2.56,0,0,0,1.41-4.7Z"/>
            <path class="cls-1"
                  d="M5.61,32.91a22.86,22.86,0,0,1-.49-4.75A23.54,23.54,0,0,1,5.5,24a2.56,2.56,0,1,0-5-.93A28.39,28.39,0,0,0,.6,34a2.56,2.56,0,0,0,2.5,2,2.39,2.39,0,0,0,.53-.06A2.56,2.56,0,0,0,5.61,32.91Z"/>
            <path class="cls-1"
                  d="M32.8,50.73a23.34,23.34,0,0,1-9,.06,2.56,2.56,0,0,0-1,5,28.31,28.31,0,0,0,11-.07,2.56,2.56,0,0,0-1-5Z"/>
            <path class="cls-1"
                  d="M44.61,44.29a23.21,23.21,0,0,1-3.53,3,2.56,2.56,0,0,0,1.44,4.68A2.61,2.61,0,0,0,44,51.48a28.23,28.23,0,0,0,4.32-3.61,2.56,2.56,0,0,0-3.66-3.58Z"/></svg>`;
                setTimeout(() => {
                    this.repasteImagesByCategory(actualCategory)
                    this.DOMloadMoreBtn.classList.remove('site-loadMore-button-no-decor');
                    this.DOMloadMoreBtn.innerHTML = normalContent;
                }, 2000)
            if (this.maxListedImagesNumber === 36 || !areEnoughImagesAvailable()) {
                this.DOMloadMoreBtn.style.display = 'none';
            }
            })
        },

        init() {
            this.initTabs();
            this.repasteImagesByCategory('all')
            this.imagesDescriptions.initGalleryListeners();
            this.initLoadMoreButtonOnclickListener();
        },

    },

    repaintTabs(tabsCollection, chosenTab) {
        [...tabsCollection]
            .find(elem => elem.classList.contains('active-tab'))
            .classList.remove('active-tab');
        chosenTab.classList.add('active-tab');
    },

    init() {
        this.nav.initOnScrollToggle();
        this.ourServices.initTabs();
        this.ourWorkSamples.init();
    },
}
page.init();
